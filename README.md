#Possible moves of knight

_Possible moves of knight_ contains a chessboard, when you choose knight position by clicking board cells, you will see possible knight moves from the chosen position. Knight position will be highlighted with blue colour and possible moves will be highlighted with green colour.

###Installation##

```$ git clone https://denisdmitriyeff@bitbucket.org/denisdmitriyeff/possible-moves-of-knight.git```

or simply upload all given files to your computer.

###How to start

Open **index.html** file in your browser.