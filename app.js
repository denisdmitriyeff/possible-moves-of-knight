function сhessBoard() {
  const chessBoard = document.getElementById('main');
  let coordinates = [];
  let cell, cellX, cellY;
  let flag = true;

  for (let i = 0; i < 8; i++) { // columns
    for (let j = 0; j < 8; j++) { // rows
      cell = document.createElement('div');
      chessBoard.appendChild(cell);

      cellX = i + 1;
      cellY = j + 1;
      coordinates.push([cellX, cellY]);

      if (j == 0) {
        flag = !flag;
      }

      if (flag) {
        cell.className = "cell black";
      } else {
        cell.className = "cell white";
      }
      flag = !flag;
    }
  }
  // add ID with coordinates to every cell
  const cells = document.getElementsByClassName('cell');
    for(let i = 0; i < cells.length; i++) {
      cells[i].setAttribute('id', coordinates[i].join(""));
    }

  document.addEventListener('click', (e) => {
    let possiblePositions = [];
    let coordinatesToShow = [];
    cell = document.querySelector('.cell');
    e.preventDefault();

    if (e.target.classList.contains('cell')) {
      // remove class from all sibling elements
      e.target.parentElement.querySelectorAll( ".cell" ).forEach( e =>
          e.classList.remove('blue'));
      // add class to the clicked element
      e.target.classList.add('blue');
      /* save clicked button coordinates in clickedX and clickedY */
      const clickedX = parseInt(e.target.getAttribute('id')[0]);
      const clickedY = parseInt(e.target.getAttribute('id')[1]);
      /* find all possible coordinates --> */
      // Find all possible X positions
      const possibleX = [clickedX - 1, clickedX - 2, clickedX + 1, clickedX + 2].filter(function(cellPosition) {
        return (cellPosition > 0 && cellPosition < 9);
      });
      // Find all possible Y postions
      const possibleY = [clickedY - 1, clickedY - 2, clickedY + 1, clickedY + 2].filter(function(cellPosition) {
        return (cellPosition > 0 && cellPosition < 9);
      });

      for (let i = 0; i < possibleX.length; i++) {
        for (let j = 0; j < possibleY.length; j++) {
          if (Math.abs(clickedX - possibleX[i]) + Math.abs(clickedY - possibleY[j]) === 3) {
            possiblePositions.push([possibleX[i], possibleY[j]]);
          }
        }
      }
      // Add valid coordinates to array
      possiblePositions.forEach( item => {
        const positions = item.join('');
        coordinatesToShow.push(positions);
      });
      console.log(coordinatesToShow);
    }
    // remove class from all cells
    document.querySelectorAll( ".cell" ).forEach( e =>
        e.classList.remove("green"));
    // add class to valid cells on the desk
    coordinatesToShow.forEach( id =>
      document.getElementById(id).classList.add("green"));
  });
} // end of сhessBoard function
сhessBoard();
